/**
 * @param {Object} defaults - The defaults object
 * @param {Object} obj - The object to merge with
 * @return {Object} The merged object
 */
module.exports = fromDefault = (defaults, obj) => {
    for (var key in obj)
        if (obj.hasOwnProperty(key))
            defaults[key] = obj[key]

    return defaults
}