# from-default
### Merging objects with style

(_Self explanatory, I guess_)
```javascript 
const fromDefault = require('from-default')

const defaults = {
    title: 'Just a boring default title',
    isCoolPackage: true
}

const obj = {
    title: 'Fancy object!'
}

console.log(fromDefault(defaults, obj))
   /* -> */ { title: 'Fancy object!', isCoolPackage: true }
```